require 'yaml'

class MetroInfopoint
  attr_reader :timetable, :lines, :line_color

  def initialize(path_to_timing_file:, path_to_lines_file:)
    @timetable = YAML.load_file(path_to_timing_file)['timing']
    stations = YAML.load_file(path_to_lines_file)
    @line_color = stations['lines']
    @lines = stations['stations']
  end

  def calculate(from_station:, to_station:)
    { price: calculate_price(from_station: from_station, to_station: to_station),
      time: calculate_time(from_station: from_station, to_station: to_station) }
  end

  def calculate_price(from_station:, to_station:)
    stations = collect_stations(from_station, to_station)
    stations.map { |stops| stops['price'] }.sum
  end

  def calculate_time(from_station:, to_station:)
    stations = collect_stations(from_station, to_station)
    stations.map { |stops| stops['time'] }.sum
  end

  def collect_stations(from_station, to_station)
    from_station, to_station = direction(from_station.to_sym, to_station.to_sym)

    from_lines = lines[from_station.to_s]
    to_lines = lines[to_station.to_s]

    common_lines = from_lines & to_lines

    if common_lines.size > 0
      without_line_change(from_station, to_station, common_lines)
    else
      with_line_change(from_station, to_station)
    end.flatten.uniq
  end

  def direction(from_station, to_station)
    from_index = line_color.index(lines[from_station.to_s].first)
    to_index = line_color.index(lines[to_station.to_s].first)
    from_index > to_index ? [to_station, from_station] : [from_station, to_station]
  end

  def without_line_change(from_station, to_station, common_lines)
    filtered_stations = []
    loop do
      stations = timetable.find do |stops|
        stops['start'] == from_station && same_line?(common_lines, stops['end'])
      end

      filtered_stations << stations
      from_station = stations['end']
      break if from_station == to_station
    end
    filtered_stations
  end

  def with_line_change(from_station, to_station)
    filtered_stations = []

    response = stops_between_crosses(from_station, to_station)
    filtered_stations << response[:stations]
    from_station = response[:next_station]

    response = find_stations(to_station, from_station, 'end')
    filtered_stations << response[:stations]
    to_station = response[:next_station]

    return filtered_stations if from_station == to_station

    filtered_stations << stops_between_crosses(from_station, to_station)[:stations]
  end

  def stops_between_crosses(next_station, end_station)
    filtered_stations = []
    start_lines = lines[next_station.to_s]
    end_lines = lines[end_station.to_s]
    common_lines = start_lines & end_lines
    line_name = if start_lines.size > 1 && common_lines.size == 0
                  end_line_index = line_color.index(end_lines[0])
                  line_color[end_line_index - 1]
                else
                  common_lines[0]
                end

    if line_name
      station_with_cross = timetable.find do |stops|
        stops['start'] == next_station &&
          lines[stops['end'].to_s].include?(line_name)
      end
      next_station = station_with_cross['end']
      filtered_stations << station_with_cross
    end

    response = find_stations(next_station, end_station, 'start')
    filtered_stations << response[:stations]

    { stations: filtered_stations, next_station: response[:next_station] }
  end

  def find_stations(next_station, end_station, first_key)
    filtered_stations = []
    second_key = (['start', 'end'] - [first_key]).first
    loop do
      stations = timetable.find do |stops|
        stops[first_key] == next_station && stops[second_key] == end_station
      end

      stations ||= timetable.find do |stops|
        stops[first_key] == next_station
      end

      filtered_stations << stations
      next_station = stations[second_key]
      break if lines[next_station.to_s].size > 1
    end
    { stations: filtered_stations, next_station: next_station }
  end

  def same_line?(road_lines, stop)
    stop_lines = lines[stop.to_s]
    road_lines.any? { |line_name| stop_lines.include?(line_name) }
  end
end
